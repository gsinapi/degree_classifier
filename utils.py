import functools
import time
import nltk
from nltk.corpus import stopwords
import geonamescache
import numpy as np
import pandas as pd
from sklearn.preprocessing import normalize
#import p_mean_FT as pmeanFT
try:
    import fasttext as fastText
except:
    import fastText as fastText
#import fasttext as fastText


def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


# set stopwords
stop_words = set(stopwords.words('english'))
stop_words.remove('it')
stop_words.remove('ma')
stop_words.remove('i')
stop_words.remove('is')
stop_words.remove('re')
stop_words.add('available')
stop_words.add('near')
stop_words.add('west')
stop_words.add('east')
stop_words.add('located')
stop_words.add('per')
stop_words.add('week')
stop_words.add('day')
stop_words.add('days')
stop_words.add('week-')
stop_words.add('immediate')
stop_words.add('annually')
stop_words.add('weekly')
stop_words.add('daily')
stop_words.add('prominent')
stop_words.add('midwest')
stop_words.add('nyc')
stop_words.add('openings')
stop_words.add('opening')
stop_words.add('downtown')
stop_words.add('remote')
stop_words.add('must')
stop_words.add('hours')
stop_words.add('hour')
stop_words.add('perm')
stop_words.add('growing')
stop_words.add('based')
stop_words.add('fast')
stop_words.add('permanent')
stop_words.add('local')
stop_words.add('required')
stop_words.add('bonus')
stop_words.add('va')
stop_words.add('beach')
stop_words.add('needed')
stop_words.add('need')
stop_words.add('needs')
stop_words.add('il')
stop_words.add('southside')
# stop_words.add('contractor')
stop_words.add('leading')
stop_words.add('inc')
stop_words.add('uk')
stop_words.add('fun')
stop_words.add('apply')
stop_words.add('donot')
stop_words.add('mn')

# list of US cities, US states and World Countries to remove
gc = geonamescache.GeonamesCache()
c = gc.get_cities()
US_cities = [c[key]['name'] for key in list(c.keys())
             if c[key]['countrycode'] == 'US']
US_cities = [x.lower() for x in US_cities]
c = gc.get_us_states()
US_states = [c[key]['name'] for key in list(c.keys())]
US_states = [x.lower() for x in US_states]
countries = gc.get_countries_by_names()
countries_list = list(countries.keys())
WORLD_COUNTRIES = [x.lower() for x in countries_list]
stop_words.update(US_cities)
stop_words.update(US_states)
stop_words.update(WORLD_COUNTRIES)
stop_words.remove('enterprise')
stop_words.remove('mobile')
STOPWORDS = stop_words

# new stopwords for names
STOPWORDS = ['phd', 'cpa', 'msa', 'ms', 'md', 'mph',
             'pe', 'pt', 'dpt', 'cscs', 'mpa', 'pmp', 'jd', 'scs', 'phr', 'mba',
             'pharmd', 'cfa', 'mbbs', 'dr', 'professor', 'prof', 'mch', 'rd', 'rn', 'rdn', 'cde',
             'ccc-slp', 'mpa', 'rn-bc', 'pmhnp-bc', 'gnp-bc', 'atc', 'NEA-BC', 'FAAN', 'educator', 'nurse', 'RMA', 'LMSW', 'edd', 'ccm', 'pac', 'cpt',
             'MHS', 'MPAS', 'MMS', 'MSHS', 'EdM', 'CCDS', 'CWS', 'CPH', 'CIC', 'RDN', 'LICSW', 'CPHQ', 'CNSC', 'ScD', 'LDN', 'CCRP', 'PMHNP-BC', 'CRNP', 'EMT-P', 'CFT',
             'MTASCP', 'CPHQ', 'MBBS', 'ma', 'CRC', 'bs', 'ches', 'CMA', 'WCS', 'PMACPT', 'cima', 'BCBA', 'LABA', 'csc', 'CRPS', 'LADC-I', 'LCDP-II', 'LMHC', 'CGMA', 'CRC',
             'CMAA', 'CEPA', 'LADCII', 'CADCII', 'CPM', 'MFE', 'CGPM', 'MA-CSL', 'MAED', 'MSIM', 'CPCU', 'AIC', 'MSW', 'LCSW-NC', 'LICSW-MA', 'lpc', 'PPS', 'RBT',
             'ASA', 'FCA', 'MAAA', 'MArch', 'NCARB', 'pmp', 'ACMA', 'CGMA', 'dma', 'ICMA-CM', 'PHR', 'SHRM-CP', 'rpa', 'fma', 'ATR-BC', 'MAATC', 'QMHP', 'PhDc', 'CAGS', 'KMOG',
             'LMFT', 'abd', 'D.ABA', 'aba', 'AHPC-SW', 'SHRM-CP', 'maat', 'AWMA', 'cfp', 'BCBA', 'CCMA', 'UMASS', 'Diploma', 'NCCAOM', 'dipl', 'MAOM', 'LMHC', 'CPhT',
             'CISA', 'CGAP', 'CRPC', 'AAMS', 'APMA', 'certified', 'cia', 'MSCJ', 'MAFF', 'CFE', 'CBE', 'CSCD', 'CCII', 'AWMA', 'AAMS', 'MBBS', 'MAWH', 'CFP', 'PsyD', 'candidate',
             'MSM-PM', 'msc', 'AFSB', 'RRT-NPS', 'AE-C', 'MSIOP', 'CDE', 'msw', 'MST', 'FNP', 'msra', 'msf', 'PMI-ACP', 'pmi', 'AGPCNP-BC', 'Regulatory', 'OCN', 'MSPT',
             'AGPCNP-BC', 'bsn', 'msn', 'CEIM', 'CPBF', 'IMH-E', 'MSEM', 'pcp', 'CGC', 'CNOR', 'CNSC', 'LDN', 'PMI-ACP', 'LABA', 'GNP-BC', 'LSSGB', 'CPAN', 'CHPN', 'MIMechE', 'RDN', 'FACS',
             'OTRP', 'OTR', 'DBA-HCML', 'LSSBB', 'IEEE', 'APRN', 'CRNP', 'HCS', 'PMACPT', 'pt', 'CMI-Spanish', 'Informatics', 'Health', 'Associate', 'AHPC-SW',
             'GISP', 'looking', 'new', 'Esq', 'CSPO', 'Nutrition', 'dnp', 'fnp-bc', 'fnpbc', 'hiring', 'im', 'PTOE', 'IMSA', 'III', 'dvm', 'dacvim', 'saim', 'cpim', 'cltd', 'pba',
             'arm', 'ais', 'aif', 'are', 'CRIS', 'CRISC', 'cpcu', 'imh', 'II', 'ing', 'dvm', 'dacvim', 'cm', 'bim', 'cmktr', 'mcim', 'mcipr', 'acmi',
             'mit', 'lmhca', 'cht', 'au', 'faafp', 'faapl', 'cpe',  'clu', 'chfc', 'ricp', 'bfa', 'aifa', 'cap', 'atp', 'ricp', 'ctfa', 'chfc', 'clu', 'caia', 'ea', 'tep',
             'Economics', 'ndtr', 'cdm', 'cfpp', 'rehs', 'rs', 'ccifp', 'cca', 'cfps', 'fm', 'mifiree', 'faafp', 'capm',  'mdt', 'ocs', 'faapl', 'fhm', 'dmd', 'assoc', 'aia',
             'med', 'bahon', 'dip', 'diploma', 'dipl', 'accounting', 'dba', 'eds', 'lnha', 'csm', 'facg', 'mse', 'emba', 'cssgb', 'DBA', 'CG', 'CGLSM', 'jr', 'cbap', 'mspm']
STOPWORDS = STOPWORDS + ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'l', 'm',
                         'n', 'o', 'p', 'q', 'r', 's', 't', 'j', 'k', 'u', 'v', 'w', 'z', 'y']
STOPWORDS = [item.lower() for item in STOPWORDS]
STOPWORDS = ['of', 'and', 'in', 'for']
STOPWORDS = set(STOPWORDS)


class P_mean:

    """
    Creates power mean concatenated word embeddings

    """
    def train_pmean(list_titles, dimension, l, tokenizer, model):
        dim = dimension

        meanlist = l  # pass list

        pFT_sent_matrix = np.empty((0, dim*len(meanlist)))
        pFT_sent2idx = {}
        pFT_idx2sent = {}

        i = 0

        for line in (list_titles):

            # indexing
            pFT_sent2idx[line] = i
            pFT_idx2sent[i] = line

            # tokenize title
            line = tokenizer(line)

            words = line.split(" ")  # split to words

            sent_vec = pmeanFT.get_sentence_embedding(
                words, model, meanlist)  # make sentence vector

            pFT_sent_matrix = np.vstack(
                (pFT_sent_matrix, sent_vec))  # add to sentence matrix
            i += 1

        pFT_sent_matrix = normalize(pFT_sent_matrix)
        return pFT_sent_matrix
