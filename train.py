# -*- coding: utf-8 -*-
# -*- author: Giovanni Sinapi -*-

"""

Train an unsupervised fasttext model from a list of strings.

"""

try:
    import fasttext as fastText
except:
    import fastText as fastText
import numpy as np
import pandas as pd
import os
import argparse
import logging
from sys import argv
import ast
import sys
from gensim.models import Word2Vec
import gensim
sys.path.append(os.getcwd())


# check if input directory exists
if os.path.isdir(os.path.join(*[os.path.curdir, 'data'])):
    print('Directory exists...')
    INPUT_FILE_NAME = os.path.join(*[os.path.curdir, 'data', 'new_preprocessed_degrees.txt'])
else:
    print('File in current directory...')
    INPUT_FILE_NAME = os.path.join(*[os.getcwd(), 'new_preprocessed_degrees.txt'])


# check if output directory exists
if os.path.isdir(os.path.join(*[os.path.curdir, 'models'])):
    OUTPUT_MODEL_NAME = os.path.join(*[os.path.curdir, 'models', 'new_ft_modelshortlong2hs.bin'])
    OUTPUT_EMBEDDINGS_NAME = os.path.join(
        *[os.path.curdir, 'models', 'new_ft_embeddingsshortlong2hs.npy'])
else:
    OUTPUT_MODEL_NAME = os.path.join(*[os.getcwd(), 'new_ft_modelshortlong2hs.bin'])
    OUTPUT_EMBEDDINGS_NAME = os.path.join(*[os.getcwd(), 'new_ft_embeddingsshortlong2hs.npy'])


def get_data(input_path):
    with open(input_path) as f:  # encoding="utf-8"
        list_titles = f.read().splitlines()
        for line in list_titles:
            yield gensim.utils.simple_preprocess(line, min_len=1, max_len=100)


print('Sentence Iterator...')
sentences = list(get_data(INPUT_FILE_NAME))


def train():
    logging.info('Training Fasttext...')

    model = fastText.train_unsupervised(input=INPUT_FILE_NAME, dim=100, epoch=200,
                                        minCount=1, wordNgrams=2, loss="ns", minn=2, maxn=6, thread=1, ws=3)
    model.save_model(OUTPUT_MODEL_NAME)
    #logging.info('Training Word2Vec...')
    # embed_model = Word2Vec(sentences, max_vocab_size=200000, workers=4,
    #                       size=100, min_count=1,
    #                       window=3, sample=1e-4, iter=10, sg=1)
    #embed_model.train(sentences, total_examples=len(sentences), epochs=50)
    #logging.info('Saving model at the following directory: %s', OUTPUT_MODEL_NAME)
    # embed_model.save('models/wordvec_model')
    return model


def create_embeddings(model):
    list_titles = sum(1 for line in open(INPUT_FILE_NAME))
    # generate emebeddings
    p_name_fasttext_128 = np.zeros((list_titles, 100))
    with open(INPUT_FILE_NAME, 'r') as f:
        for i, name in enumerate(f):
            p_name_fasttext_128[i] = model.get_sentence_vector(name.rstrip('\n'))
    # save embeddings
    logging.info('Saving embeddings at the following directory: %s', OUTPUT_EMBEDDINGS_NAME)
    np.save(OUTPUT_EMBEDDINGS_NAME, p_name_fasttext_128)


if __name__ == '__main__':
    model = train()
    create_embeddings(model)

# python -u train.py
