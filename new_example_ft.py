"""
Train and evaluate and test model

TODO: custom urlify beginning and inference
NEW Embeddings: fasttext
word tokenizer

"""

from keras.models import load_model
from keras_han.layers import AttentionLayer
import re
import numpy as np
import pandas as pd
import logging
import sys
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.utils import to_categorical
from nltk.tokenize import sent_tokenize
from sklearn.model_selection import train_test_split
from keras_han.model import HAN
import numpy as np
from gensim.models import word2vec
import keras.backend as K
from sklearn.metrics import classification_report, confusion_matrix
import math
from keras.optimizers import Adam
from keras.regularizers import l2
import pickle
try:
    import fasttext as fastText
except:
    import fastText as fastText
from matplotlib import pyplot as plt
import seaborn as sns


# Create a logger to provide info on the state of the
# script
stdout = logging.StreamHandler(sys.stdout)
stdout.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
))
logger = logging.getLogger('default')
logger.setLevel(logging.INFO)
logger.addHandler(stdout)

MAX_WORDS_PER_SENT = 20
MAX_SENT = 1
MAX_VOC_SIZE = 200000
GLOVE_DIM = 100
TEST_SPLIT = 0.1
SAVE = False  # Save word_tokenizer
SAVE_DEGREES = False  # True for new data to preprocess


def precision(y_true, y_pred):
    """Precision metric.
    Only computes a batch-wise average of precision.
    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def recall(y_true, y_pred):
    """Recall metric.
    Only computes a batch-wise average of recall.
    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        """Recall metric.
        Only computes a batch-wise average of recall.
        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.
        Only computes a batch-wise average of precision.
        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


precision.__name__ = 'precision'
recall.__name__ = 'recall'
f1.__name__ = 'f1'


#####################################################
# Pre processing (add urlify)                                   #
#####################################################
logger.info("Pre-processsing data.")


#data = pd.read_csv('data/degree_level_annotated_set.csv')
data = pd.read_csv('data/degree_level_annotated_set_corrected.csv')
data['degree_level'] = data['degree_level'].astype(int)
#dg_levels = data.degree_level.unique()
#dg_dict = dict(zip(dg_levels, range(len(dg_levels))))
#data['degree_level'] = data['degree_level'].map(dg_dict)
data['degree_level'] = data['degree_level'].map(
    {12: 0, 13: 1, 14: 2, 16: 3, 17: 4, 18: 5, 19: 6, 21: 7})
target = data['degree_level'].values
list_degrees = [e for e in data['degree']]


if SAVE_DEGREES:
    logger.info("Saving raw degree list.")
    outF = open('data/list_degrees_new.txt', "w")
    with open('data/list_degrees_new.txt', 'a') as f:
        for s in list_degrees:
            f.write(s + '\n')
        f.close()

#####################################################
# run preprocess.py and train.py        #
#####################################################


def get_data(input_path):
    with open(input_path) as f:  # encoding="utf-8"
        list_titles = f.read().splitlines()
    return list_titles


reviews = get_data('data/new_preprocessed_degrees.txt')

#####################################################
# Tokenization                                      #
#####################################################
logger.info("Tokenization.")

# Build a Keras Tokenizer that can encode every token


word_tokenizer = Tokenizer(num_words=MAX_VOC_SIZE)
word_tokenizer.fit_on_texts(reviews)

if SAVE:
    with open('embeddings/word_tokenizer_new.pickle', 'wb') as handle:
        pickle.dump(word_tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
        logger.info("Saved tokenizer.")

# Construct the input matrix. This should be a nd-array of
# shape (n_samples, MAX_SENT, MAX_WORDS_PER_SENT).
# We zero-pad this matrix (this does not influence
# any predictions due to the attention mechanism.
X = np.zeros((len(reviews), MAX_SENT, MAX_WORDS_PER_SENT), dtype='int32')

for i, review in enumerate(reviews):
    sentences = sent_tokenize(review)
    tokenized_sentences = word_tokenizer.texts_to_sequences(
        sentences
    )
    tokenized_sentences = pad_sequences(
        tokenized_sentences, maxlen=MAX_WORDS_PER_SENT
    )

    pad_size = MAX_SENT - tokenized_sentences.shape[0]

    if pad_size < 0:
        tokenized_sentences = tokenized_sentences[0:MAX_SENT]
    else:
        tokenized_sentences = np.pad(
            tokenized_sentences, ((0, pad_size), (0, 0)),
            mode='constant', constant_values=0
        )

    # Store this observation as the i-th observation in
    # the data matrix
    X[i] = tokenized_sentences[None, ...]

# Transform the labels into a format Keras can handle
y = to_categorical(target, num_classes=8)

# We make a train/test split
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, shuffle=True)
#X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.1, random_state=1)

X_train, X_rest, y_train, y_rest = train_test_split(X, y,
                                                    test_size=0.1,  # 0.1, 0.15, 0.2
                                                    stratify=y,
                                                    random_state=42)
X_val, X_test, y_val, y_test = train_test_split(X_rest, y_rest,
                                                test_size=0.5,
                                                stratify=y_rest,
                                                random_state=42)

#####################################################
# Word Embeddings                                   #
#####################################################
logger.info(
    "Creating embedding matrix"
)


def load_fastext_new(embed_file, normalized=False):
    embedding_model = fastText.load_model(embed_file)
    embeddings = {}
    embedding_matrix = np.random.random(
        (len(word_tokenizer.word_index) + 1, GLOVE_DIM)
    )
    embedding_matrix[0] = 0
    for word, index in word_tokenizer.word_index.items():
        embedding_vector = embedding_model.get_word_vector(word)
        if embedding_vector is not None:
            embedding_matrix[index] = embedding_vector
    return embedding_matrix


embedding_matrix = load_fastext_new('models/new_ft_modelshortlong2hs.bin')


def load_w2v_new(embed_file, normalized=False):
    embeddings = {}
    embedding_matrix = np.random.random(
        (len(word_tokenizer.word_index) + 1, GLOVE_DIM)
    )
    embedding_model = word2vec.Word2Vec.load(embed_file)
    embedding_matrix[0] = 0
    for word, index in word_tokenizer.word_index.items():
        try:
            embedding_vector = embedding_model.wv.get_vector(word)
            if embedding_vector is not None:
                embedding_matrix[index] = embedding_vector
        except:
            pass

    return embedding_matrix


#embedding_matrix = load_w2v_new('models/wordvec_model')

#####################################################
# Model Training                                    #
#####################################################

# 256, 256 ft and w2v


def train_evaluate():
    logger.info("Training the model.")
    han_model = HAN(
        MAX_WORDS_PER_SENT, MAX_SENT, 8, embedding_matrix,
        word_encoding_dim=512, sentence_encoding_dim=512
    )

    han_model.summary()

    han_model.compile(
        optimizer=Adam(clipnorm=1.0, lr=0.001), loss='categorical_crossentropy',
        metrics=['categorical_accuracy',  precision, recall, f1]
    )

    es = EarlyStopping(monitor='val_loss', mode='auto', verbose=1, patience=3)
    checkpoint_saver = ModelCheckpoint(
        filepath='models/model_512512bnnotrainft.{epoch:02d}-{val_loss:.2f}.hdf5',
        verbose=1, save_best_only=True, monitor='val_loss'
    )

    han_model.fit(
        X_train, y_train, batch_size=64, epochs=30,
        validation_data=(X_test, y_test),
        # validation_split=0.1,
        callbacks=[es, checkpoint_saver]
    )

    logger.info("Evaluating the model.")
    results = han_model.evaluate(
        X_val, y_val, batch_size=64, verbose=1
    )
    print(results)
    labels = ['12', '13', '14', '16', '17', '18', '19', '21']
    # labels = list(dg_levels)
    predictions = han_model.predict(X_val)
    matrix = confusion_matrix(y_val.argmax(axis=1), predictions.argmax(axis=1))
    print(matrix)
    cm = matrix
    cm_df = pd.DataFrame(cm,
                         index=labels,
                         columns=labels)
    plt.figure(figsize=(5.5, 4))
    sns.heatmap(cm_df, annot=True, cmap='Blues', fmt='g', cbar=False)
    plt.title('Confusion Matrix \nModel:{}'.format('HAN_FT'))
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig('data/han_512512bnnotrainft.png')
    print(classification_report(y_val.argmax(axis=1), predictions.argmax(axis=1), target_names=labels))
    clsf_report = pd.DataFrame(classification_report(y_val.argmax(axis=1), predictions.argmax(
        axis=1), target_names=labels, output_dict=True)).transpose()
    clsf_report.to_csv('data/han_512512bnnotrainft.csv')

    logger.info("Predict")
    labels = ['12', '13', '14', '16', '17', '18', '19', '21']
    predd = han_model.predict(X[:1])
    zipped = zip(labels, predd[0])
    print(sorted(zipped, key=lambda elem: elem[1], reverse=True))
