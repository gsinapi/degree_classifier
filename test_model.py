"""
Test model

TODO: custom urlify beginning and inference
NEW Embeddings: fasttext

"""

from keras.models import load_model
from keras_han.layers import AttentionLayer
import re
import numpy as np
import pandas as pd
import logging
import sys
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.utils import to_categorical
from nltk.tokenize import sent_tokenize
from sklearn.model_selection import train_test_split
from keras_han.model import HAN
import numpy as np
from gensim.models import word2vec
import keras.backend as K
from sklearn.metrics import classification_report, confusion_matrix
import math
from keras.optimizers import Adam
from keras.regularizers import l2
import pickle

# Create a logger to provide info on the state of the
# script
stdout = logging.StreamHandler(sys.stdout)
stdout.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
))
logger = logging.getLogger('default')
logger.setLevel(logging.INFO)
logger.addHandler(stdout)

MAX_WORDS_PER_SENT = 20
MAX_SENT = 1
MAX_VOC_SIZE = 200000
GLOVE_DIM = 100
TEST_SPLIT = 0.1
SAVE = False


def precision(y_true, y_pred):
    """Precision metric.
    Only computes a batch-wise average of precision.
    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def recall(y_true, y_pred):
    """Recall metric.
    Only computes a batch-wise average of recall.
    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        """Recall metric.
        Only computes a batch-wise average of recall.
        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.
        Only computes a batch-wise average of precision.
        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


precision.__name__ = 'precision'
recall.__name__ = 'recall'
f1.__name__ = 'f1'


logger.info('Predict new sentences')
han = load_model('models/model.07-final.hdf5', custom_objects={
    'HAN': HAN,
    'AttentionLayer': AttentionLayer,
    'precision': precision,
    'recall': recall,
    'f1': f1
})

with open('embeddings/word_tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)


def predict_from_text(reviews):
    '''
    predict_from_text(['visiting student'])
    TODO: add urlify at the beginning

    '''
    labels = ['12', '13', '14', '16', '17', '18', '19', '21']
    X = np.zeros((len(reviews), MAX_SENT, MAX_WORDS_PER_SENT), dtype='int32')

    for i, review in enumerate(reviews):
        sentences = sent_tokenize(review)
        tokenized_sentences = tokenizer.texts_to_sequences(
            sentences
        )
        tokenized_sentences = pad_sequences(
            tokenized_sentences, maxlen=MAX_WORDS_PER_SENT
        )

        pad_size = MAX_SENT - tokenized_sentences.shape[0]

        if pad_size < 0:
            tokenized_sentences = tokenized_sentences[0:MAX_SENT]
        else:
            tokenized_sentences = np.pad(
                tokenized_sentences, ((0, pad_size), (0, 0)),
                mode='constant', constant_values=0
            )

        # Store this observation as the i-th observation in
        # the data matrix
        X[i] = tokenized_sentences[None, ...]

    predd = han.predict(X)
    zipped = zip(labels, predd[0])
    return sorted(zipped, key=lambda elem: elem[1], reverse=True)
