"""
Train and evaluate and test model

TODO: custom urlify beginning and inference
NEW Embeddings: fasttext
word tokenizer

"""

from keras.models import load_model
from keras_han.layers import AttentionLayer
import re
import numpy as np
import pandas as pd
import logging
import sys
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.utils import to_categorical
from nltk.tokenize import sent_tokenize
from sklearn.model_selection import train_test_split
from keras_han.model import HAN
import numpy as np
from gensim.models import word2vec
import keras.backend as K
from sklearn.metrics import classification_report, confusion_matrix
import math
from keras.optimizers import Adam
from keras.regularizers import l2
import pickle

# Create a logger to provide info on the state of the
# script
stdout = logging.StreamHandler(sys.stdout)
stdout.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
))
logger = logging.getLogger('default')
logger.setLevel(logging.INFO)
logger.addHandler(stdout)

MAX_WORDS_PER_SENT = 20
MAX_SENT = 1
MAX_VOC_SIZE = 200000
GLOVE_DIM = 100
TEST_SPLIT = 0.1
SAVE = False


def precision(y_true, y_pred):
    """Precision metric.
    Only computes a batch-wise average of precision.
    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def recall(y_true, y_pred):
    """Recall metric.
    Only computes a batch-wise average of recall.
    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        """Recall metric.
        Only computes a batch-wise average of recall.
        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.
        Only computes a batch-wise average of precision.
        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


precision.__name__ = 'precision'
recall.__name__ = 'recall'
f1.__name__ = 'f1'


#####################################################
# Pre processing (add urlify)                                   #
#####################################################
logger.info("Pre-processsing data.")

# Load Kaggle's IMDB example data
data = pd.read_csv('data/degree_level_annotated_set.csv')


# Do some basic cleaning of the review text
def remove_quotations(text):
    """
    Remove quotations and slashes from the dataset.
    """
    text = re.sub(r"\\", "", text)
    text = re.sub(r"\'", "", text)
    text = re.sub(r"\"", "", text)
    return text


def remove_html(text):
    """
    Very, very raw parser to remove HTML tags from
    texts.
    """
    tags_regex = re.compile(r'<.*?>')
    return tags_regex.sub('', text)


data['degree'] = data['degree'].apply(remove_quotations)
data['degree'] = data['degree'].apply(remove_html)
data['degree'] = data['degree'].apply(lambda x: x.strip().lower())

# Get the data and the sentiment
reviews = data['degree'].values
data['degree_level'] = data['degree_level'].astype(int)
data['degree_level'] = data['degree_level'].map(
    {12: 0, 13: 1, 14: 2, 16: 3, 17: 4, 18: 5, 19: 6, 21: 7})
# target = data['degree_level'].astype(str)
target = data['degree_level'].values
del data


#####################################################
# Tokenization                                      #
#####################################################
logger.info("Tokenization.")

# Build a Keras Tokenizer that can encode every token


word_tokenizer = Tokenizer(num_words=MAX_VOC_SIZE)
word_tokenizer.fit_on_texts(reviews)

if SAVE:
    with open('embeddings/word_tokenizer.pickle', 'wb') as handle:
        pickle.dump(word_tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
        logger.info("Saved tokenizer.")

# Construct the input matrix. This should be a nd-array of
# shape (n_samples, MAX_SENT, MAX_WORDS_PER_SENT).
# We zero-pad this matrix (this does not influence
# any predictions due to the attention mechanism.
X = np.zeros((len(reviews), MAX_SENT, MAX_WORDS_PER_SENT), dtype='int32')

for i, review in enumerate(reviews):
    sentences = sent_tokenize(review)
    tokenized_sentences = word_tokenizer.texts_to_sequences(
        sentences
    )
    tokenized_sentences = pad_sequences(
        tokenized_sentences, maxlen=MAX_WORDS_PER_SENT
    )

    pad_size = MAX_SENT - tokenized_sentences.shape[0]

    if pad_size < 0:
        tokenized_sentences = tokenized_sentences[0:MAX_SENT]
    else:
        tokenized_sentences = np.pad(
            tokenized_sentences, ((0, pad_size), (0, 0)),
            mode='constant', constant_values=0
        )

    # Store this observation as the i-th observation in
    # the data matrix
    X[i] = tokenized_sentences[None, ...]

# Transform the labels into a format Keras can handle
y = to_categorical(target, num_classes=8)

# We make a train/test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, shuffle=True)

X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.1, random_state=1)

#####################################################
# Word Embeddings                                   #
#####################################################
logger.info(
    "Creating embedding matrix"
)


def load_fastext_vocab(embed_file, normalized=False):
    embedding_model = fasttext.FastText.load(embed_file)
    word_index = dict([(k, v.index) for k, v in embedding_model.wv.vocab.items()])
    embedding_matrix = embedding_model.wv.syn0
    if normalized:
        embedding_matrix = embedding_model.wv.syn0 / \
            np.sqrt((embedding_model.wv.syn0 ** 2).sum(-1))[:, np.newaxis].astype(float)

    return word_index, embedding_matrix


def load_w2v(embed_file, normalized=False):
    embedding_model = word2vec.Word2Vec.load(embed_file)
    word_index = dict([(k, v.index) for k, v in embedding_model.wv.vocab.items()])
    embedding_matrix = embedding_model.wv.syn0
    if normalized:
        embedding_matrix = embedding_model.wv.syn0 / \
            np.sqrt((embedding_model.wv.syn0 ** 2).sum(-1))[:, np.newaxis].astype(float)

    return word_index, embedding_matrix

# vocabulary, embedding_matrix = load_w2v('embeddings/100_embeddings')


def load_w2v_new(embed_file, normalized=False):
    embeddings = {}
    with open('embeddings/glove.6B.%dd.txt' % GLOVE_DIM, encoding='utf-8') as file:
        for line in file:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')

            embeddings[word] = coefs
    embedding_matrix = np.random.random(
        (len(word_tokenizer.word_index) + 1, GLOVE_DIM)
    )
    embedding_model = word2vec.Word2Vec.load(embed_file)
    embedding_matrix[0] = 0
    for word, index in word_tokenizer.word_index.items():
        try:
            embedding_vector = embedding_model.wv.get_vector(word)
            if embedding_vector is not None:
                embedding_matrix[index] = embedding_vector
        except:
            embedding_vector = embeddings.get(word)
            if embedding_vector is not None:
                embedding_matrix[index] = embedding_vector
    return embedding_matrix

# embedding_matrix = load_w2v('embeddings/100_embeddings')


# Load the embeddings from a file
embeddings = {}
with open('embeddings/glove.6B.%dd.txt' % GLOVE_DIM, encoding='utf-8') as file:
    for line in file:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')

        embeddings[word] = coefs

# Initialize a matrix to hold the word embeddings
embedding_matrix = np.random.random(
    (len(word_tokenizer.word_index) + 1, GLOVE_DIM)
)

# Let the padded indices map to zero-vectors. This will
# prevent the padding from influencing the results
embedding_matrix[0] = 0

# Loop though all the words in the word_index and where possible
# replace the random initalization with the GloVe vector.
for word, index in word_tokenizer.word_index.items():
    embedding_vector = embeddings.get(word)
    if embedding_vector is not None:
        embedding_matrix[index] = embedding_vector


# use combined glove + word2vec pretrained
embedding_matrix = load_w2v_new('embeddings/100_embeddings')

#####################################################
# Model Training                                    #
#####################################################


def train_evaluate():
    logger.info("Training the model.")

    han_model = HAN(
        MAX_WORDS_PER_SENT, MAX_SENT, 8, embedding_matrix,
        word_encoding_dim=100, sentence_encoding_dim=256
    )

    han_model.summary()

    han_model.compile(
        optimizer=Adam(clipnorm=1.0, lr=0.001), loss='categorical_crossentropy',
        metrics=['categorical_accuracy',  precision, recall, f1]
    )

    es = EarlyStopping(monitor='val_loss', mode='auto', verbose=1, patience=3)
    checkpoint_saver = ModelCheckpoint(
        filepath='models/model.{epoch:02d}-{val_loss:.2f}.hdf5',
        verbose=1, save_best_only=True, monitor='val_loss'
    )

    han_model.fit(
        X_train, y_train, batch_size=64, epochs=30,
        validation_data=(X_test, y_test),
        # validation_split=0.1,
        callbacks=[es, checkpoint_saver]
    )

    logger.info("Evaluating the model.")
    results = han_model.evaluate(
        X_val, y_val, batch_size=64, verbose=1
    )
    print(results)
    labels = ['12', '13', '14', '16', '17', '18', '19', '21']
    predictions = han_model.predict(X_val)
    matrix = confusion_matrix(y_val.argmax(axis=1), predictions.argmax(axis=1))
    print(matrix)
    print(classification_report(y_val.argmax(axis=1), predictions.argmax(axis=1), target_names=labels))

    logger.info("Predict")
    labels = ['12', '13', '14', '16', '17', '18', '19', '21']
    predd = han_model.predict(X[:1])
    zipped = zip(labels, predd[0])
    print(sorted(zipped, key=lambda elem: elem[1], reverse=True))


logger.info('Predict new sentences')
han = load_model('models/model.07-final.hdf5', custom_objects={
    'HAN': HAN,
    'AttentionLayer': AttentionLayer,
    'precision': precision,
    'recall': recall,
    'f1': f1
})

with open('embeddings/word_tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)


def predict_from_text(reviews):
    '''
    predict_from_text(['visiting student'])
    TODO: add urlify at the beginning

    '''
    labels = ['12', '13', '14', '16', '17', '18', '19', '21']
    X = np.zeros((len(reviews), MAX_SENT, MAX_WORDS_PER_SENT), dtype='int32')

    for i, review in enumerate(reviews):
        sentences = sent_tokenize(review)
        tokenized_sentences = tokenizer.texts_to_sequences(
            sentences
        )
        tokenized_sentences = pad_sequences(
            tokenized_sentences, maxlen=MAX_WORDS_PER_SENT
        )

        pad_size = MAX_SENT - tokenized_sentences.shape[0]

        if pad_size < 0:
            tokenized_sentences = tokenized_sentences[0:MAX_SENT]
        else:
            tokenized_sentences = np.pad(
                tokenized_sentences, ((0, pad_size), (0, 0)),
                mode='constant', constant_values=0
            )

        # Store this observation as the i-th observation in
        # the data matrix
        X[i] = tokenized_sentences[None, ...]

    predd = han.predict(X)
    zipped = zip(labels, predd[0])
    return sorted(zipped, key=lambda elem: elem[1], reverse=True)
