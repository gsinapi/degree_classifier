# -*- coding: utf-8 -*-
# -*- author: Giovanni Sinapi -*-

"""

Prepare dataset for training. Preprocess the data and apply some logic
to avoid to remove and split acronyms and special characters where needed.

"""

from nltk.corpus import stopwords
from utils import STOPWORDS as stop_words
import re
import nltk
import pandas as pd
import numpy as np
import ast
import os
import argparse
import logging
from sys import argv
import sys
sys.path.append(os.getcwd())
try:
    import fasttext as fastText
except:
    import fastText as fastText


# check if the data directory exists
if os.path.isdir(os.path.join(*[os.path.curdir, 'data'])):
    print('Directory exists...')
    OUTPUT_FILE_NAME = os.path.join(*[os.path.curdir, 'data', 'new_preprocessed_degrees.txt'])
else:
    print('Saving in current directory...')
    OUTPUT_FILE_NAME = os.path.join(*[os.getcwd(), 'new_preprocessed_degrees.txt'])


# Create a short custom list that contains words to not tokenize.
list_acr = ['M&A', 'P&C', 'FP&A', 'H&B', 'R&D', 'L&D',
            'F&B', 'I&C', 'O&M', 'F&I', 'I&E', 'L&I', 'E&I', 'Pm&R', 'C&A', 'A/C', 'E/O', 'A/V', 'A/B', 'A/C'
            'E-Commerce', 'Co-op', 'On-', 'In-', 'Full-Stack', 'e-', 'co-',
            '.Net', 'c#', 'c++', 'k-', 'business-to', 'f#', 'cdl-', 'a&p']
list_acr = [item.lower() for item in list_acr]


def urlify(s):
    """Custom tokenizer

    It handles abbreviations, acronyms, special symbols.

    Parameters
    ----------
    s: str
       Input string to preprocess

    Returns
    -------
    final sentence or s: str
        Preprocessed string

    Examples
    --------
    >>> s = 'A/P Analyst - Downtown Boston $70k'
    >>> bgt.urlify(s)


    """

    #s = replace_levels(s)
    #s = find_replace_multi_ordered(s, self.dict)

    s = s.lower()
    s = ' '.join([word for word in s.split() if not any(
        [phrase in word for phrase in ['$']])])
    #s = re.sub(r"\b12\b", "12th", s)

    if any(ext in s for ext in list_acr):
        s = s.lower()
        tokens = s.split()
        final_list_tokens = []
        for token in tokens:
            if any(ext in token for ext in list_acr):
                final_list_tokens.append(token)
            else:
                list_spec = ['&', '/']
                if any(ext in token for ext in list_spec):
                    special_char = [
                        ext for ext in list_spec if(ext in token)][0]
                    if (any(ext in special_char for ext in ['&', '/']) and (len(token.split(special_char)[0]) == 1 and len(token.split(special_char)[1]) == 1)):
                        final_list_tokens.append(token)
                    else:
                        token = token.lower()
                        token = token.replace("/", " ")
                        token = token.replace("-", " ")
                        token = token.replace("&", " ")
                        token = token.replace("_", " ")
                        token = token.replace(",", " ")
                        token = re.sub(r"[^\w\s]", '', token)
                        #token = re.sub(r"\b\d+\b", "", token)
                        final_list_tokens.append(token)
                else:
                    token = token.lower()
                    token = token.replace("/", " ")
                    token = token.replace("-", " ")
                    token = token.replace("&", " ")
                    token = token.replace("_", " ")
                    token = token.replace(",", " ")
                    token = re.sub(r"[^\w\s]", '', token)
                    #token = re.sub(r"\b\d+\b", "", token)
                    final_list_tokens.append(token)
        final_sentence = ' '.join(final_list_tokens)
        final_sentence = ' '.join(
            [word for word in final_sentence.split() if word not in stop_words])
        # Replace all runs of whitespace with a single dash
        final_sentence = re.sub(r"\s+", ' ', final_sentence)
        return final_sentence
    else:
        list_spec = ['&', '/']
        if any(ext in s for ext in list_spec):
            s = s.lower()
            tokens = s.split()
            final_list_tokens = []
            for token in tokens:
                if any(ext in token for ext in list_spec):
                    special_char = [
                        ext for ext in list_spec if(ext in token)][0]
                    if (any(ext in special_char for ext in ['&', '/']) and (len(token.split(special_char)[0]) == 1 and len(token.split(special_char)[1]) == 1)):
                        final_list_tokens.append(token)
                    else:
                        token = token.lower()
                        token = token.replace("/", " ")
                        token = token.replace("-", " ")
                        token = token.replace("&", " ")
                        token = token.replace("_", " ")
                        token = token.replace(",", " ")
                        # Remove all non-word characters (everything except numbers and letters)
                        token = re.sub(r"[^\w\s]", '', token)
                        #token = re.sub(r"\b\d+\b", "", token)
                        final_list_tokens.append(token)
                else:
                    token = token.lower()
                    token = token.replace("/", " ")
                    token = token.replace("-", " ")
                    token = token.replace("&", " ")
                    token = token.replace("_", " ")
                    token = token.replace(",", " ")
                    # Remove all non-word characters (everything except numbers and letters)
                    token = re.sub(r"[^\w\s]", '', token)
                    #token = re.sub(r"\b\d+\b", "", token)
                    final_list_tokens.append(token)

            final_sentence = ' '.join(final_list_tokens)
            final_sentence = ' '.join(
                [word for word in final_sentence.split() if word not in stop_words])
            # Replace all runs of whitespace with a single dash
            final_sentence = re.sub(r"\s+", ' ', final_sentence)
            return final_sentence

        else:
            s = s.lower()
            s = s.replace("/", " ")
            s = s.replace("-", " ")
            s = s.replace("&", " ")
            s = s.replace("_", " ")
            s = s.replace(",", " ")
            # Remove all non-word characters (everything except numbers and letters)
            s = re.sub(r"[^\w\s]", '', s)
            #s = re.sub(r"\b\d+\b", "", s)
            s = ' '.join([word for word in s.split()
                          if word not in stop_words])
            # Replace all runs of whitespace with a single dash
            s = re.sub(r"\s+", ' ', s)
            return s


def run(input_path):
    with open(input_path) as f:
        list_titles = f.read().splitlines()
    logging.info('Saving file at the following directory: %s', OUTPUT_FILE_NAME)
    outF = open(OUTPUT_FILE_NAME, "w")
    with open(OUTPUT_FILE_NAME, 'a') as f:
        for s in list_titles:
            f.write(urlify(s) + '\n')
        f.close()
    logging.info('File saved!')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_path', help='Input path')
    args = parser.parse_args()
    logging.basicConfig(format='[%(asctime)s] [%(levelname)s] %(message)s', level=logging.INFO)
    run(args.input_path)


if __name__ == '__main__':
    main()

# python -u preprocess_1.py data/list_titles.txt
