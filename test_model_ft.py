"""
Test model

TODO: custom urlify beginning and inference
NEW Embeddings: fasttext

"""

from keras.models import load_model
from keras_han.layers import AttentionLayer, AttentionWithContext, TrigPosEmbedding, SelfAttention
import re
import numpy as np
import pandas as pd
import logging
import sys
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.utils import to_categorical
from nltk.tokenize import sent_tokenize
from sklearn.model_selection import train_test_split
from keras_han.model import HAN
import numpy as np
from gensim.models import word2vec
import keras.backend as K
from sklearn.metrics import classification_report, confusion_matrix
import math
from keras.optimizers import Adam
from keras.regularizers import l2
import pickle
try:
    import fasttext as fastText
except:
    import fastText as fastText
from preprocess import urlify
import multiprocessing as mp
import multiprocessing.pool

# Create a logger to provide info on the state of the
# script
stdout = logging.StreamHandler(sys.stdout)
stdout.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
))
logger = logging.getLogger('default')
logger.setLevel(logging.INFO)
logger.addHandler(stdout)

MAX_WORDS_PER_SENT = 20
MAX_SENT = 1
MAX_VOC_SIZE = 200000
GLOVE_DIM = 100


def precision(y_true, y_pred):
    """Precision metric.
    Only computes a batch-wise average of precision.
    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def recall(y_true, y_pred):
    """Recall metric.
    Only computes a batch-wise average of recall.
    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        """Recall metric.
        Only computes a batch-wise average of recall.
        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.
        Only computes a batch-wise average of precision.
        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


precision.__name__ = 'precision'
recall.__name__ = 'recall'
f1.__name__ = 'f1'


logger.info('Predict new sentences')
# model_ft or modelW2v: model_ft.03-0.02 (100-512 - Best) or model_w2v512.03-0.03 (512) or model_ftnew512.08-0.02 (512-512) or model_ft_bn.07-0.02 (100-512 bn)
# model_ft_highway.06-0.02 (100-512 highway) or model_ft_att.05-0.02 (100-512 att context sentence)
# 1) 100 - 512 bn (model_100512bn_.06-0.01.hdf5) 0.10 ft
# 2) 512 - 512 bn (model_512512bn_.02-0.01)   BEST 1 !! 0.10 ft
# 3) 512 - 512 bn pos (model_512512bnpos_.08-0.02) 0.10 ft
# 4) 512 - 512 bn w2v (model_512512bnw2v.06-0.02)    0.10 BEST 2 !! w2v
# 5) 1024 - 1024 bn w2v (model_10241024bnw2v.04-0.02)    0.10 w2v
# 6)  256 - 256 bn ft (model_256256bnft.08-0.01)  0.10 ft
# 7)  256 - 256 bn w2v (model_256256bnw2v.06-0.02)  0.10  w2v
# 8) 512 -512 bibigru bn ft (model_512512bibigruft.07-0.01)  0.10  ft
# 9) 512 -512 bibigru bn w2v (model_512512bibigruw2v.06-0.02)  0.10  w2v
# 10) 512 -512 lstm bn ft (model_512512lstmft.04-0.01)  0.10  ft
# 11) 512 -512 lstm bn w2v (model_512512lstmw2v.06-0.02)  0.10  w2v
# 12) 512 -512 bibilstm bn (model_512512bibilstmft.11-0.02)  0.15  ft
# 13 512 - 512 bn embed not trainable (model_512512bnnotrainft.04-0.01) 0.10 ft
han = load_model('models/model_512512bn_.02-0.01.hdf5', custom_objects={
    'HAN': HAN,
    'AttentionLayer': AttentionLayer,
    'precision': precision,
    'recall': recall,
    'f1': f1
})

with open('embeddings/word_tokenizer_new.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)


def predict_from_text(reviews):
    '''
    predict_from_text(['visiting student'])

    '''
    labels = ['12', '13', '14', '16', '17', '18', '19', '21']
    X = np.zeros((len(reviews), MAX_SENT, MAX_WORDS_PER_SENT), dtype='int32')

    for i, review in enumerate(reviews):
        review = urlify(review)
        sentences = sent_tokenize(review)
        tokenized_sentences = tokenizer.texts_to_sequences(
            sentences
        )
        tokenized_sentences = pad_sequences(
            tokenized_sentences, maxlen=MAX_WORDS_PER_SENT
        )

        pad_size = MAX_SENT - tokenized_sentences.shape[0]

        if pad_size < 0:
            tokenized_sentences = tokenized_sentences[0:MAX_SENT]
        else:
            tokenized_sentences = np.pad(
                tokenized_sentences, ((0, pad_size), (0, 0)),
                mode='constant', constant_values=0
            )

        # Store this observation as the i-th observation in
        # the data matrix
        X[i] = tokenized_sentences[None, ...]

    predd = han.predict(X)
    zipped = zip(labels, predd[0])
    return sorted(zipped, key=lambda elem: elem[1], reverse=True)


def predict_from_string(review):
    '''
    predict_from_text('visiting student')

    '''
    labels = ['12', '13', '14', '16', '17', '18', '19', '21']
    X = np.zeros((1, MAX_SENT, MAX_WORDS_PER_SENT), dtype='int32')

    review = urlify(review)
    sentences = sent_tokenize(review)
    tokenized_sentences = tokenizer.texts_to_sequences(
        sentences
    )
    tokenized_sentences = pad_sequences(
        tokenized_sentences, maxlen=MAX_WORDS_PER_SENT
    )

    pad_size = MAX_SENT - tokenized_sentences.shape[0]

    if pad_size < 0:
        tokenized_sentences = tokenized_sentences[0:MAX_SENT]
    else:
        tokenized_sentences = np.pad(
            tokenized_sentences, ((0, pad_size), (0, 0)),
            mode='constant', constant_values=0
        )

    # Store this observation as the i-th observation in
    # the data matrix
    X[0] = tokenized_sentences[None, ...]

    predd = han.predict(X)
    zipped = zip(labels, predd[0])
    return sorted(zipped, key=lambda elem: elem[1], reverse=True)


def run_goldset(file_name):
    try:
        data = pd.read_excel(file_name)
    except:
        data = pd.read_csv(file_name)

    print(len(data))
    data['raw_degree'] = data['raw_degree'].astype(str)

    df = pd.DataFrame()
    for i in range(len(data)):
        # id_job=data['LIJobID'][i]
        text_job = data['raw_degree'][i]
        print(i)
        pred = predict_from_text([text_job])
        df = df.append(
            {'raw_degree': text_job, 'degree_level': pred[:1][0][0], 'prob': pred[:1][0][1]}, ignore_index=True)
    df = df[['raw_degree', 'degree_level', 'prob']]
    df.to_csv('data/goldset_results_han_512512bnnotrainft.csv', index=False)
    print('Finished')


def run_full_set(file_name):
    '''
    %time df = run_full_set('data/complete_degree_count.parquet')

    '''
    import pyarrow.parquet as pq
    import pyarrow
    data = pq.read_table(file_name).to_pandas()

    print(len(data))
    data['raw_degree'] = data['raw_degree'].astype(str)

    data = data[:3000000]
    print(len(data))

    df = pd.DataFrame()
    for i in range(len(data)):
        # id_job=data['LIJobID'][i]
        text_job = data['raw_degree'][i]
        print(i)
        pred = predict_from_string(text_job)
        df = df.append(
            {'raw_degree': text_job, 'degree_level': pred[:1][0][0], 'prob': pred[:1][0][1]}, ignore_index=True)
    df = df[['raw_degree', 'degree_level', 'prob']]
    #df = pd.merge(df, data, on='raw_degree')
    # save csv
    #df.to_csv('data/degree_full_set_100000.csv', index=False)
    # save parquet
    try:
        print('start saving...')
        schema = pyarrow.Schema.from_pandas(df, preserve_index=False)
        parquet_writer = pq.ParquetWriter(
            'data/degree_mapping_complete_top_3m.parquet', schema=schema)
        parquet_writer.write_table(pyarrow.Table.from_pandas(df,
                                                             preserve_index=False))
        parquet_writer.close()
        print('File Saved')
    except:
        print('File too large')
    print('Finished')
    return df


def process_complete(file_name, limit=100):
    '''
    %time df = process_complete('data/complete_degree_count.parquet')
    try
    '''
    import pyarrow.parquet as pq
    import pyarrow
    data = pq.read_table(file_name).to_pandas()

    print(len(data))
    data['raw_degree'] = data['raw_degree'].astype(str)

    data = data[:limit]
    print(len(data))

    result = []

    for i in range(len(data)):
        print(i)
        # breakpoint()

        temp = predict_from_string(data['raw_degree'][i])
        temp = pd.DataFrame(temp, columns=['degree_level', 'prob'])[:1]
        temp["raw_degree"] = data['raw_degree'][i]

        result.append(temp)

    return pd.concat(result, axis=0, ignore_index=True)

# def get_sentence_attention(reviews):
#     '''
#     return attention weight to sentence
#
#     '''
#     labels = ['12', '13', '14', '16', '17', '18', '19', '21']
#     X = np.zeros((len(reviews), MAX_SENT, MAX_WORDS_PER_SENT), dtype='int32')
#
#     for i, review in enumerate(reviews):
#         review = urlify(review)
#         sentences = sent_tokenize(review)
#         tokenized_sentences = tokenizer.texts_to_sequences(
#             sentences
#         )
#         tokenized_sentences = pad_sequences(
#             tokenized_sentences, maxlen=MAX_WORDS_PER_SENT
#         )
#
#         pad_size = MAX_SENT - tokenized_sentences.shape[0]
#
#         if pad_size < 0:
#             tokenized_sentences = tokenized_sentences[0:MAX_SENT]
#         else:
#             tokenized_sentences = np.pad(
#                 tokenized_sentences, ((0, pad_size), (0, 0)),
#                 mode='constant', constant_values=0
#             )
#
#         # Store this observation as the i-th observation in
#         # the data matrix
#         X[i] = tokenized_sentences[None, ...]
#
#     predd = HAN.predict_sentence_attention(han, X)
#     return predd


def block_indices(n, n_blocks):

    block_size = int(n / n_blocks)

    block_indices = np.zeros(n_blocks + 1, dtype=int)

    rest = n % n_blocks

    for i in range(rest + 1):

        block_indices[i] = (block_size + 1) * i

    for i in range(rest + 1, n_blocks + 1):

        block_indices[i] = block_indices[i - 1] + block_size

    return block_indices


def iterate(func, n, processes=mp.cpu_count(), args=()):

    indices = block_indices(n, processes)

    parameters = [(indices[i], indices[i + 1], *args) for i in range(indices.size - 1)]

    with mp.pool.Pool(processes) as pool:

        return pool.starmap(func, parameters)


def process_block(first, last, titles):

    result = []

    for i in range(first, last):
        print(i)
        # breakpoint()

        temp = predict_from_string(titles[i])
        temp = pd.DataFrame(temp, columns=['degree_level', 'prob'])[:1]
        temp["raw_degree"] = titles[i]

        result.append(temp)

    return pd.concat(result, axis=0, ignore_index=True)


def titles_to_suboccs(titles,
                      processes=mp.cpu_count()):

    args = (titles, )

    return pd.concat(iterate(process_block, len(titles), processes, args), axis=0,
                     ignore_index=True)


def get_input(input_file, limit):
    import pyarrow.parquet as pq
    import pyarrow
    data = pq.read_table(input_file).to_pandas()

    print(len(data))
    data['raw_degree'] = data['raw_degree'].astype(str)

    list_new_titles = [str(e) for e in data['raw_degree']]
    return list_new_titles[:limit]


#list_new_titles = get_input('data/complete_degree_count.parquet', 100)
#df = titles_to_suboccs(list_new_titles, processes=2)
#test_dt = df[['title', 'BGTSubOccName', 'weighted_score']]

def get_input_complete(input_file, limit):
    import pyarrow.parquet as pq
    import pyarrow
    data = pq.read_table(input_file).to_pandas()
    data = data[13000000:]
    print(len(data))
    data['raw_degree'] = data['raw_degree'].astype(str)

    list_new_titles = [str(e) for e in data['raw_degree']]
    return list_new_titles[:limit]


def run_full_set_complete(file_name, limit=100):
    '''
    %time df = run_full_set_complete('data/complete_degree_count.parquet', 100000000)

    '''
    import pyarrow.parquet as pq
    import pyarrow
    list_new_titles = get_input_complete(file_name, limit)
    print(len(list_new_titles))
    df = process_block(0, len(list_new_titles), list_new_titles)
    #df = df[['raw_degree', 'degree_level', 'prob']]

    try:
        schema = pyarrow.Schema.from_pandas(df, preserve_index=False)
        parquet_writer = pq.ParquetWriter(
            'data/degree_mapping_complete_top_13_endmillion.parquet', schema=schema)
        parquet_writer.write_table(pyarrow.Table.from_pandas(df,
                                                             preserve_index=False))
        parquet_writer.close()
        print('File Saved')
    except:
        print('File too large')
    print('Finished')
    return df
