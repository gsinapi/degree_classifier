### FIRST MODEL:
- glove + word2vec + default tokenizer
- train: %run new_example -> train_evaluate()
- test: %run test_model -> predict_from_text(['visiting student'])

### SECOND MODEL:
- fasttext/word2vec + urlify
- %run new_example_ft to produce new data and save list_new_degrees (SAVE_DEGREES=Yes)
- python -u preprocess.py data/list_titles.txt (preprocess using urlify)
- python -u train.py (train fasttext/word2vec)
- train: %run new_example_ft -> save word_tokenizer (SAVE=YES) -> train_evaluate()
- test: %run test_model_ft -> predict_from_text(['visiting student'])
